//
//  ApiService.swift
//  poke-list
//
//  Created by Mr APPs on 30/11/2020.
//

import UIKit

typealias VoidBlock = () -> Void
typealias StringBlock = (_ success: Bool, _ message: String?) -> Void
typealias DataBlock = (_ success: Bool, _ data: Any?, _ message: String?) -> Void
typealias ValueBlock = (_ success: Bool, _ data: [String : Any]?, _ message: String?) -> Void

class ApiService: NSObject {

    private let baseUrl = "https://pokeapi.co/api/v2"
    
    func getPokemons(offset: Int, limit: Int, completion: @escaping ValueBlock) {
        request(path: "pokemon?limit=\(limit)&offset=\(offset)") { (success, data, message) in
            if success, let data = data as? [String : Any] {
                completion(true, data, nil)
            } else {
                completion(false, nil, message)
            }
        }
    }
    
    func getPokemonDetail(pokemonId: Int, completion: @escaping DataBlock) {
        request(path: "pokemon/\(pokemonId)", completion: completion)
    }
    
    private func request(path: String, completion: @escaping DataBlock) {
        let stringUrl = "\(baseUrl)/\(path)"
        let url = URL(string: stringUrl)!
        
        print("[ApiService] Request: \(url.absoluteString)")
        URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
            
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    print("[ApiService] Response Json: \(json)")
                    completion(true, json, nil)
                } catch {
                    completion(false, nil, error.localizedDescription)
                }
            } else if let error = error {
                completion(false, nil, error.localizedDescription)
            } else {
                completion(false, nil, nil)
            }
            
        }.resume()
    }
}
