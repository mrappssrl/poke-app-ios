//
//  DetailViewController.swift
//  poke-list
//
//  Created by Mr APPs on 11/12/2020.
//

import UIKit

enum Section: Int {
    case stats = 0
    case moves = 1
}

class DetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    private var tableView: UITableView!
    private var imgSpriteDefault: UIImageView!
    private var imgSpriteShiny: UIImageView!
    private var lblTypes: UILabel!
    private var model: DetailModel!
    private let cellIdentifier = "cellIdentifier"
    
    convenience init(pokemon: Pokemon) {
        self.init()
        setupModel(pokemon: pokemon)
    }
    
    func setupModel(pokemon: Pokemon) {
        model = DetailModel(pokemon: pokemon, onUpdate: {
            self.updateSprite()
            self.updateTypes()
            self.tableView.reloadData()
        })
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = model.pokemon?.name.capitalized
        view.backgroundColor = "background".color
        
        tableView = UITableView(frame: view.bounds, style: .grouped)
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        let footer = UIView()
        footer.backgroundColor = .clear
        tableView.tableFooterView = footer
        
        let header = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 180))
        header.backgroundColor = .clear
        imgSpriteDefault = UIImageView(frame: CGRect(x: 0, y: 0, width: header.frame.size.width/2, height: 150))
        imgSpriteDefault.contentMode = .scaleAspectFit
        header.addSubview(imgSpriteDefault)
        imgSpriteShiny = UIImageView(frame: CGRect(x: header.frame.size.width/2, y: 0, width: header.frame.size.width/2, height: imgSpriteDefault.frame.size.height))
        imgSpriteShiny.contentMode = .scaleAspectFit
        header.addSubview(imgSpriteShiny)
        
        lblTypes = UILabel(frame: CGRect(x: 0, y: 150, width: header.frame.size.width, height: 30))
        lblTypes.textAlignment = .center
        header.addSubview(lblTypes)
        
        tableView.tableHeaderView = header
        
        tableView.backgroundColor = "background".color
        tableView.separatorColor = "separator".color
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 44
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        
        model.getPokemonDetail()
    }
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return model.pokemon.stats.count > 0 ? 2 : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == Section.stats.rawValue ? model.pokemon.stats.count : model.pokemon.moves.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == Section.stats.rawValue ? "Stats" : "Moves"
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: cellIdentifier)
        cell.backgroundColor = "cellBackground".color
        
        if indexPath.section == Section.stats.rawValue {
            let stat = model.pokemon.stats[indexPath.row]
            cell.textLabel?.text = stat.name
            cell.detailTextLabel?.text = "\(stat.baseValue)"
        } else {
            let move = model.pokemon.moves[indexPath.row]
            cell.textLabel?.text = move.name
            cell.detailTextLabel?.text = nil
        }
        return cell
    }
    
    // MARK: - Layout updates
    
    func updateSprite() {
        if self.imgSpriteDefault != nil {
            self.imgSpriteDefault.loadImage(from: model.pokemon.spriteFrontDefault)
        }
        if self.imgSpriteShiny != nil {
            self.imgSpriteShiny.loadImage(from: model.pokemon.spriteFrontShiny)
        }
    }
    
    func updateTypes() {
        if model.pokemon.typeB != "" {
            lblTypes.text = "[\(model.pokemon.typeA)] - [\(model.pokemon.typeB)]"
        } else {
            lblTypes.text = "[\(model.pokemon.typeA)]"
        }
    }

}
