//
//  HomeViewController.swift
//  poke-list
//
//  Created by Mr APPs on 30/11/2020.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    private var tableView: UITableView!
    private var model: HomeModel!
    private let cellIdentifier = "cellIdentifier"
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
            super.init(nibName: nil, bundle: nil)

        setupModel()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setupModel()
    }
    
    func setupModel() {
        model = HomeModel(onUpdate: {
            DispatchQueue.main.async {
                if self.tableView != nil {
                    self.tableView.reloadData()
                }
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Poké List"
        view.backgroundColor = "background".color
        
        tableView = UITableView()
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        let footer = UIView()
        footer.backgroundColor = .clear
        tableView.tableFooterView = footer
        
        tableView.backgroundColor = "background".color
        tableView.separatorColor = "separator".color
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = 60
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        
        model.getPokemons()
    }

    // MARK: - UITableView DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.pokemons.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == model.pokemons.count - 4 {
            model.getPokemons()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)

        let pokemon = model.pokemons[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        cell.backgroundColor = "cellBackground".color
        cell.textLabel?.text = "\(pokemon.id). \(pokemon.name.capitalized)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let pokemon = model.pokemons[indexPath.row]
        navigationController?.pushViewController(DetailViewController(pokemon: pokemon), animated: true)
    }
}
