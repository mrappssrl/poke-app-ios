//
//  Extensions.swift
//  poke-list
//
//  Created by Mr APPs on 10/12/2020.
//

import Foundation
import UIKit

extension UIView {
    
    class var lateralMargin : CGFloat {
        return 12.0
    }
}

extension String {
    
    var color : UIColor? {
        return UIColor(named: self)
    }
}

extension UIImageView {
    
    func loadImage(from stringUrl: String) {
        if let url = URL(string: stringUrl) {
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url)
                DispatchQueue.main.async {
                    if let data = data {
                        self.image = UIImage(data:data)
                    } else{
                        self.image = nil
                    }
                }
            }
        }
    }
}
