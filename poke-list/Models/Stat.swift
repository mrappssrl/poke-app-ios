//
//  Move.swift
//  poke-list
//
//  Created by Mr APPs on 11/12/2020.
//

import RealmSwift

class Stat: Object {
    
    @objc dynamic var id: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var baseValue: Int = 0

    override static func primaryKey() -> String? {
        return "id"
    }
    
    class func from(json: [String:Any], pokemonId: Int) -> Stat {
        
        let stat = Stat()
        if let value = json["base_stat"] as? Int {
            stat.baseValue = value
        }
        if let parsedStat = json["stat"] as? [String:Any] {
            if let name = parsedStat["name"] as? String {
                stat.name = name.capitalized
                stat.id = "\(pokemonId)_\(name)"
            }
        }
        return stat
    }
}
