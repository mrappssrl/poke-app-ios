//
//  Move.swift
//  poke-list
//
//  Created by Mr APPs on 11/12/2020.
//

import RealmSwift

class Move: Object {
    
    @objc dynamic var name: String = ""

    override static func primaryKey() -> String? {
        return "name"
    }
    
    class func from(json: [String:Any]) -> Move {
        
        let move = Move()
        if let parsedMove = json["move"] as? [String:Any] {
            if let name = parsedMove["name"] as? String {
                move.name = name.capitalized
            }
        }
        return move
    }
}
