//
//  Pokemon.swift
//  poke-list
//
//  Created by Mr APPs on 11/12/2020.
//

import RealmSwift

class Pokemon: Object {

    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var spriteFrontDefault: String = ""
    @objc dynamic var spriteFrontShiny: String = ""
    @objc dynamic var typeA: String = ""
    @objc dynamic var typeB: String = ""
    let stats = List<Stat>()
    let moves = List<Move>()
    
    class func from(json: [String:Any]) -> Pokemon {
        
        let pokemon = Pokemon()
        if let id = json["id"] as? Int {
            pokemon.id = id
        }
        if let name = json["name"] as? String {
            pokemon.name = name
        }
        if let sprites = json["sprites"] as? [String:Any] {
            if let frontDefault = sprites["front_default"] as? String {
                pokemon.spriteFrontDefault = frontDefault
            }
            if let frontShiny = sprites["front_shiny"] as? String {
                pokemon.spriteFrontShiny = frontShiny
            }
        }
        if let types = json["types"] as? [[String:Any]] {
            if let first = types.first {
                if let type = first["type"] as? [String:Any] {
                    if let typeName = type["name"] as? String {
                        pokemon.typeA = typeName.capitalized
                    }
                }
                if types.count > 1 {
                    if let last = types.last {
                        if let type = last["type"] as? [String:Any] {
                            if let typeName = type["name"] as? String {
                                pokemon.typeB = typeName.capitalized
                            }
                        }
                    }
                }
            }
        }
        if let moves = json["moves"] as? [[String:Any]] {
            for move in moves {
                pokemon.moves.append(Move.from(json: move))
            }
        }
        if let stats = json["stats"] as? [[String:Any]] {
            for stat in stats {
                pokemon.stats.append(Stat.from(json: stat, pokemonId: pokemon.id))
            }
        }
        
        return pokemon
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
