//
//  HomeModel.swift
//  poke-list
//
//  Created by Mr APPs on 30/11/2020.
//

import UIKit
import RealmSwift

class HomeModel: NSObject {

    private var apiService: ApiService!
    private var offset: Int = 0
    private let limit: Int = 20
    private var didFinishPaging: Bool = false
    private var isLoading: Bool = false
    var updateController : (() -> ()) = {}
    var pokemons : Results<Pokemon>!
    var notificationToken: NotificationToken?
    
    convenience init(onUpdate: @escaping (() -> ()) = {}) {
        self.init()
        
        self.updateController = onUpdate
        setup()
    }
    
    override init() {
        super.init()
        setup()
    }
    
    func setup() {
        apiService = ApiService()
        let realm = try! Realm()
        pokemons = realm.objects(Pokemon.self).sorted(byKeyPath: "id", ascending: true)
        offset = pokemons.count
        notificationToken = pokemons.observe({ (changes) in
            self.updateController()
        })
    }
    
    deinit {
        notificationToken?.invalidate()
        notificationToken = nil
    }
    
    func refresh() {
        offset = 0
        didFinishPaging = false
        getPokemons()
    }
    
    func getPokemons() {
        if isLoading || didFinishPaging {
            return
        }
        
        apiService.getPokemons(offset: offset, limit: limit) { (success, data, message) in
            if let data = data {
                    self.addPokemons(data: data)
            }
        }
    }
    
    func getPokemonDetail(pokemonIndex: Int, completion: @escaping DataBlock) {
        apiService.getPokemonDetail(pokemonId: pokemonIndex, completion: completion)
    }
    
    func addPokemons(data: [String : Any]) {
        guard let results = data["results"] as? [[String : Any]] else { return }
        
        var index = offset
        
        let realm = try! Realm()
        try! realm.write {
            
            var newPokemons = [Pokemon]()
            
            for object in results {
                
                var copy = object
                copy["id"] = index+1
                let pokemon = Pokemon.from(json: copy)
                newPokemons.append(pokemon)
                index += 1
            }
            
            if newPokemons.count > 0 {
                realm.add(newPokemons, update: .all)
            }
        }
        
        offset += results.count
        didFinishPaging = results.count < limit
    }
}
