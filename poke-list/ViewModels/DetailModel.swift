//
//  DetailModel.swift
//  poke-list
//
//  Created by Mr APPs on 11/12/2020.
//

import UIKit
import RealmSwift

class DetailModel: NSObject {

    var pokemon: Pokemon!
    private var apiService: ApiService!
    var updateController : (() -> ()) = {}
    
    init(pokemon: Pokemon, onUpdate: @escaping (() -> ()) = {}) {
        self.pokemon = pokemon
        self.apiService = ApiService()
        self.updateController = onUpdate
    }
    
    func getPokemonDetail() {
        apiService.getPokemonDetail(pokemonId: pokemon.id) { (success, data, message) in
            if let detail = data as? [String:Any] {
                DispatchQueue.main.async {
                    let realm = try! Realm()
                    try! realm.write {
                        let pokemon = Pokemon.from(json: detail)
                        realm.add(pokemon, update: .all)
                        self.pokemon = pokemon
                        self.updateController()
                    }
                }
            }
        }
    }
}
