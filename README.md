
## Dependencies

The only dependency used is Realm Database, for a faster and cleaner usage of the local database, class-based, easy to use and with good performances.

## Extras

Dark mode support
